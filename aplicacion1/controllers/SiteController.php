<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // esta es la accion que se ejecuta por defecto
        return $this->render('index'); // renderiza la vista index
    }

    
    


    

    public function actionMensaje(){
        return $this->render('mostrarMensaje');
    }

    public function actionImagen($id=0){
        $imagenes=[
            'foto1.jpg',
            'foto2.jpg',
            'foto3.jpg',
        ];
        // la foto que me muestra la vista la recibe por parametro
        return $this->render('imagen',[
            'foto'=>$imagenes[$id],
        ]);
    
    }

    public function actionDias(){
        // creo un array con los dias de la semana
        $dias=['lunes','martes','miercoles','jueves','viernes','sabado','domingo'];
        return $this->render('dias',[
            'datos'=>$dias]
        );
    }  

    public function actionMeses(){
        $datos=[

'enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'
        ];
        return $this->render('meses',[
            'meses'=>$datos
            
            
        ]);
    }

    public function actionImagenes(){
// esto lo leeria desde una Base de datos
        $imagenes=[
            'foto1.jpg',
            'foto2.jpg',
            'foto3.jpg',
        ];
        // la foto que me muestra la vista la recibe por parametro
        return $this->render('imagenes',[
            'foto'=>$imagenes,
        ]);
    }

    public function actionAccion1(){
        // recoge los datos del modelo o de un formulario
        // y los trata
        // y devolverle los resultados a la vista

        $numero=1;
        $numero1=2;
        $suma=$numero+$numero1;

        $datos=[1,3,5,7,9
        ];
        $mayor=max($datos);

        $texto="Ejemplo de clase";
        // contar las letra e
        $e=substr_count(strtolower($texto),'e');
        
        
        return $this->render('accion1',[
            'numeros'=>[$numero,$numero1],
            'suma'=>$suma,
            'datos'=>$datos,
            'mayor'=>$mayor,
            'texto'=>$texto,
            'numeroE'=>$e

        ]);
    }
}
    
